﻿using LabsGame.Input.Actions;
using UnityEngine;
using UnityEngine.InputSystem;

namespace LabsGame.Player {
    public class PlayerController : MonoBehaviour {
        private PlayerActions playerActions;

        public Transform verticalRotationTransform;
        public Transform horizontalRotationTransform;
        public Rigidbody movementRigidbody;

        public float verticalSensitivity = 0.1f;
        public float horizontalSensitivity = 0.1f;

        public float acceleration = 30;
        public float deceleration = 30;
        public float maxSpeed = 5;

        public float minVerticalAngle = -90;
        public float maxVerticalAngle = 90;

        private float verticalRotation = 0;

        private void OnEnable() {
            if (playerActions == null) {
                playerActions = new PlayerActions();
                playerActions.Movement.Look.performed += OnLook;
                playerActions.Enable();
            }
        }

        private void FixedUpdate() {
            Vector2 movement = playerActions.Movement.Move.ReadValue<Vector2>();

            if (movementRigidbody.velocity.magnitude < maxSpeed) {
                Vector3 addedVelocity = new Vector3(movement.x * acceleration * Time.fixedDeltaTime, 0, movement.y * acceleration * Time.fixedDeltaTime);
                movementRigidbody.AddRelativeForce(addedVelocity, ForceMode.VelocityChange);
                Debug.Log(movementRigidbody.velocity.magnitude);
            }

            movementRigidbody.AddRelativeForce(-movementRigidbody.velocity * (Mathf.Clamp01(deceleration * Time.deltaTime) * Time.deltaTime), ForceMode.VelocityChange);
        }

        private void OnLook(InputAction.CallbackContext context) {
            Vector2 lookDelta = context.ReadValue<Vector2>();

            horizontalRotationTransform.Rotate(Vector3.up, lookDelta.x * horizontalSensitivity);

            verticalRotation = Mathf.Clamp(verticalRotation - lookDelta.y * verticalSensitivity, minVerticalAngle, maxVerticalAngle);
            verticalRotationTransform.localEulerAngles = new Vector3(verticalRotation, 0, 0);
        }
    }
}